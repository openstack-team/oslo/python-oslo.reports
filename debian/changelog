python-oslo.reports (3.5.1-2) experimental; urgency=medium

  * d/watch: switch to version=4 and mode=git.

 -- Thomas Goirand <zigo@debian.org>  Tue, 25 Feb 2025 16:30:54 +0100

python-oslo.reports (3.5.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 24 Feb 2025 17:27:33 +0100

python-oslo.reports (3.4.0-3) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090576).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 08:35:44 +0100

python-oslo.reports (3.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Sep 2024 17:58:07 +0200

python-oslo.reports (3.4.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Aug 2024 17:16:34 +0200

python-oslo.reports (3.3.0-3) unstable; urgency=medium

  * Drop the extraneous dependency on python3-six (Closes: #1070185).

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 May 2024 08:31:11 +0200

python-oslo.reports (3.3.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 03 Apr 2024 16:42:30 +0200

python-oslo.reports (3.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Add python3-oslo.config as depends.
  * Removed python3-eventlet from build-depends.

 -- Thomas Goirand <zigo@debian.org>  Sat, 24 Feb 2024 22:35:58 +0100

python-oslo.reports (3.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Oct 2023 13:56:51 +0200

python-oslo.reports (3.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Aug 2023 14:46:23 +0200

python-oslo.reports (3.0.0-3) unstable; urgency=medium

  * Cleans better (Closes: #1048972).

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Aug 2023 13:06:03 +0200

python-oslo.reports (3.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 11:41:28 +0200

python-oslo.reports (3.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Feb 2023 11:15:02 +0100

python-oslo.reports (2.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2022 13:55:10 +0200

python-oslo.reports (2.4.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 29 Aug 2022 16:42:37 +0200

python-oslo.reports (2.3.0-3) unstable; urgency=medium

  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Mar 2022 08:56:04 +0100

python-oslo.reports (2.3.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 16:53:20 +0200

python-oslo.reports (2.3.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 23 Aug 2021 15:45:20 +0200

python-oslo.reports (2.2.0-3) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 09:15:14 +0200

python-oslo.reports (2.2.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2020 10:02:55 +0200

python-oslo.reports (2.2.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 13 Sep 2020 10:51:00 +0200

python-oslo.reports (2.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Sep 2020 21:30:43 +0200

python-oslo.reports (2.0.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 22:18:52 +0200

python-oslo.reports (2.0.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 07 Apr 2020 15:44:28 +0200

python-oslo.reports (1.30.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 00:45:11 +0200

python-oslo.reports (1.30.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Added python3-sphinxcontrib.apidoc to build-depends.

 -- Thomas Goirand <zigo@debian.org>  Thu, 12 Sep 2019 09:01:33 +0200

python-oslo.reports (1.29.2-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 17 Jul 2019 01:14:49 +0200

python-oslo.reports (1.29.2-1) experimental; urgency=medium

  * New upstream release.
  * Removed Python 2 support.

 -- Thomas Goirand <zigo@debian.org>  Wed, 20 Mar 2019 23:46:18 +0100

python-oslo.reports (1.28.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs

  [ Thomas Goirand ]
  * Uploading to unstable.
  * Added missing subunit and testrepository build-depends.

 -- Thomas Goirand <zigo@debian.org>  Mon, 03 Sep 2018 23:14:59 +0200

python-oslo.reports (1.28.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Switched to pgkos-dh_auto_test and blacklist one test which appears
    obviously broken (but the non-test code looks fine).

 -- Thomas Goirand <zigo@debian.org>  Mon, 20 Aug 2018 01:21:34 +0200

python-oslo.reports (1.26.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 21:26:41 +0000

python-oslo.reports (1.26.0-1) experimental; urgency=medium

  * Set VCS URLs to point to salsa.
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Standards-Version is now 4.1.3.

 -- Thomas Goirand <zigo@debian.org>  Sun, 11 Feb 2018 11:28:45 +0000

python-oslo.reports (1.22.0-2) unstable; urgency=medium

  * Uploading to unstable
  * Fixed minimum version for openstackdocstheme (Closes: #880398).

 -- Thomas Goirand <zigo@debian.org>  Tue, 31 Oct 2017 09:47:43 +0000

python-oslo.reports (1.22.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Updating standards version to 4.0.1.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Using pkgos-dh_auto_install.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 Sep 2017 17:03:26 +0000

python-oslo.reports (1.7.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Apr 2016 10:13:22 +0000

python-oslo.reports (1.7.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 02 Apr 2016 12:43:58 +0000

python-oslo.reports (1.6.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Fixed homepage (https).
  * Fixed VCS URLs (https).

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Standards-Version: 3.9.7 (no change).

 -- Thomas Goirand <zigo@debian.org>  Thu, 03 Mar 2016 02:11:12 +0000

python-oslo.reports (1.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed build-depends versions.
  * Fixed debian/copyright ordering.

 -- Thomas Goirand <zigo@debian.org>  Fri, 15 Jan 2016 07:12:17 +0000

python-oslo.reports (1.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Also test with Py3.5.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 Jan 2016 06:36:18 +0000

python-oslo.reports (1.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 01 Dec 2015 11:51:33 +0100

python-oslo.reports (0.5.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2015 20:31:55 +0000

python-oslo.reports (0.5.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Only test with python 3.4 for now.

 -- Thomas Goirand <zigo@debian.org>  Sun, 27 Sep 2015 18:34:39 +0200

python-oslo.reports (0.3.0-1) unstable; urgency=medium

  * Initial release. (Closes: #794167)

 -- Thomas Goirand <zigo@debian.org>  Fri, 31 Jul 2015 00:18:51 +0200
